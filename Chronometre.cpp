﻿/*
 * Chronometre.cpp
 * This file is part of ChronoCheckMate
 *
 * Copyright (C) 2010 - François Drouhard
 *
 * ChronoCheckMate is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * ChronoCheckMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ChronoCheckMate; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */
 
#include "Chronometre.h"
#include <QTimer>
#include <QtWidgets>
#include <QLabel>
#include <QObject>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QFont>
#include <QTime>
#include <QGroupBox>
#include <QDebug>

Chronometre::Chronometre (const QString & nom, Couleur unecouleur, QTime const& tempsDepart, Placement placement, QWidget * parent) :
    QWidget (parent),
    couleur(unecouleur),
    temps(tempsDepart),
    msTempsDepart(tempsDepart),
    nb_coups (0)
{
    QHBoxLayout *layoutPrincipal = new QHBoxLayout;
    labelPion = new QLabel;
    pion = new QPixmap();
    setCouleur(couleur);
    labelPion->setPixmap(*pion);

    groupBox = new QGroupBox(nom);
    groupBox->setFlat(false);
    groupBox->setFont(QFont(groupBox->font().family(), 15));

    QFont font("Arial", 45);

    ecranTemps = new QLabel;
    ecranTemps->setMinimumWidth(200);
    ecranTemps->setFont(font);
    ecranTemps->setDisabled(true);

    labelCoups = new QLabel;

    ecranLayout = new QVBoxLayout;
    //ecranLayout->addSpacing(10);
    //ecranLayout->addStretch(1);
    ecranLayout->addWidget(ecranTemps, 1, Qt::AlignHCenter);
    ecranLayout->addWidget(labelCoups, 0);
    //ecranLayout->addStretch(1);
    //ecranLayout->addSpacing(10);

    if (placement == Placement::Gauche)
    {
        layoutPrincipal->addWidget(labelPion,0);
        layoutPrincipal->addWidget(groupBox,1);
    } else
    {
        layoutPrincipal->addWidget(groupBox,1);
        layoutPrincipal->addWidget(labelPion,0);
    }
    groupBox->setLayout (ecranLayout);
    setLayout(layoutPrincipal);
	
    timer = new QTimer  (this);
    timer->setInterval (INTERVAL);
	QObject::connect (timer , &QTimer::timeout , this , &Chronometre::refresh);
}

void Chronometre::modifierNomJoueur(QString const& joueur) {
    groupBox->setTitle(joueur);
}

void Chronometre::definirTemps(QTime const& tempsDepart) {
    msTempsDepart = tempsDepart;
}

void Chronometre::echangerCouleur(Chronometre & chrono) {
    Couleur temp = couleur;
    setCouleur(chrono.couleur);
    chrono.setCouleur(temp);
}

void Chronometre::setCouleur (Couleur const& color) {
    couleur = color;
    QString stringCouleur;
    if (couleur == Blanc)
        stringCouleur = "white";
    else
        stringCouleur = "black";
    pion->load(QString (":/ressources/pawn_%1.svg").arg(stringCouleur));
    labelPion->setPixmap(*pion);
}

Couleur Chronometre::getCouleur() const {
    return couleur;
}

void Chronometre::refresh () {
    temps = temps.addMSecs(-INTERVAL);
    affichage ();
    if (temps <= QTime (0,0,0,0)) {
        emit fin(this);
    }
}

void Chronometre::start () {
    ecranTemps->setEnabled(true);
    nb_coups++;
	timer->start ();
}

void Chronometre::stop () {
    ecranTemps->setDisabled(true);
    timer->stop();
}


void Chronometre::affichage () {
    QString chaineTemps = temps.toString("hh:mm:ss");
    QString coups;
    ecranTemps->setText(chaineTemps);
    labelCoups->setText("Coup : " + coups.setNum(nombre_coups()));
    //qDebug() << "Taille : " << ecranTemps->width();
}

void Chronometre::reinit () {
	if (timer->isActive()) {
		timer->stop();
	}
    temps  = msTempsDepart;
    nb_coups = 0;
	affichage();
}

void Chronometre::basculer () {
	if (timer->isActive()) {
        stop();
        emit is_paused(this);
	}
	else {
        start ();
	}
}

void Chronometre::declareGagnant()
{
    emit won(this);
}

void Chronometre::basculer(Chronometre & Chrono) {
    basculer();
    Chrono.basculer();
}

QString Chronometre::nomJoueur() const
{
    return groupBox->title();
}

/**
 * @brief Ajouter du temps, prévu pour plus que des secondes
 */
void Chronometre::addSecs(unsigned int secondes)
{
    temps = temps.addSecs(secondes);
}

/**
 * @brief Nombre de coups
 * @return uint du nombre de coups
 */
uint Chronometre::nombre_coups() const
{
    return nb_coups;
}