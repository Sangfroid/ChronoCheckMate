#ifndef OPTIONS_H
#define OPTIONS_H

#include <QApplication>
#include <QTime>
#include <QSettings>

class QString;

class Options : public QSettings
{

public:
    explicit Options (QObject *parent = nullptr);
    virtual ~Options();
    void setTempsDepart (QTime const& temps);
    void setNomJoueur1 (QString const& joueur);
    void setNomJoueur2 (QString const& joueur);
    void setSon (bool son);
    void setTempsSupplementaire(bool isTempsSupp);
    void setTempsSupplementaire(unsigned int secondes);
    QString nomJoueur1 () const;
    QString nomJoueur2 () const;
    QTime tempsDepart () const;
    unsigned int tempsSupplementaire() const;
    bool isTempsSupplementaire() const;
    bool son () const;

public slots:
    void chargerConf();
    void sauverConf();

signals:

private:
    QTime           m_tempsDepart;
    QString         m_nomJoueur1;
    QString         m_nomJoueur2;
    bool            m_son;
    bool            m_isTempsSupplementaire;
    unsigned int    m_tempsSupplementaire;

};

#endif // OPTIONS_H
