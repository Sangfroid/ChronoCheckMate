/*
 * MaFenetre.cpp
 * This file is part of ChronoCheckMate
 *
 * Copyright (C) 2010 - François Drouhard
 *
 * ChronoCheckMate is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * ChronoCheckMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ChronoCheckMate; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */
 
#include "MaFenetre.h"
#include "Chronometre.h"
#include "options.h"
#include "Dialog.h"
#include <QMenuBar>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QKeyEvent>
#include <QApplication>
#include <QLabel>
#include <QDialog>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QMessageBox>
#include <QSound>

MaFenetre::MaFenetre() :
    QMainWindow (),
    marche(false),
    option(new Options(this))
{
    restoreGeometry(option->value("MAIN/geometry").toByteArray());
    QWidget *centralWidget = new QWidget;

    chrono1 = new Chronometre(option->nomJoueur1(), Blanc, option->tempsDepart(), Placement::Gauche, this);
    chrono2 = new Chronometre(option->nomJoueur2(), Noir, option->tempsDepart(), Placement::Droite, this);
    boutonInverser = new QPushButton ("<->");
    boutonChangerCouleur = new QPushButton("Changer de couleur");
    boutonInverser->setMinimumWidth(50);
    boutonInverser->setMaximumWidth(50);

    ////////////////// Layout Boutons inversion /////////////////////////////////
    QVBoxLayout *layoutBoutonsInverser = new QVBoxLayout;
    layoutBoutonsInverser->addStretch(1);
    layoutBoutonsInverser->addWidget(boutonInverser, 0, Qt::AlignCenter);
    layoutBoutonsInverser->addWidget(boutonChangerCouleur, 0, Qt::AlignCenter);
    layoutBoutonsInverser->addSpacing(10);

	////////////////// Groupe Définition de la classe ///////////////////////////
    layoutChrono = new QHBoxLayout;
    layoutChrono->addWidget(chrono1);
    layoutChrono->addLayout(layoutBoutonsInverser);
    layoutChrono->addWidget(chrono2);

    ///////////////////////////// Menus ////////////////////////////
    QMenu *menuFichier = menuBar()->addMenu("&Fichier");
    QAction *actionNouveau = new QAction ("&Nouveau" , this);
    QAction *actionOptions = new QAction ("&Options...", this);
    QAction *actionQuitter = new QAction ("&Quitter" , this);
    menuFichier->addAction(actionNouveau);
    menuFichier->addAction(actionOptions);
    menuFichier->addSeparator();
    menuFichier->addAction(actionQuitter);

	///////////////////////////// Boutons   /////////////////////////////
    boutonDemarrer = new QPushButton ("Commencer (Barre Espace)");
    boutonArreter = new QPushButton ("Arrêter(Touche Entrée)");
	
	boutonArreter->setFocusPolicy (Qt::NoFocus);
    boutonInverser->setFocusPolicy(Qt::NoFocus);
    boutonChangerCouleur->setFocusPolicy(Qt::NoFocus);
	
	layoutBoutonSwitch = new QHBoxLayout;
    layoutBoutonSwitch->addWidget (boutonDemarrer,0, Qt::AlignLeft);
    layoutBoutonSwitch->addWidget (boutonArreter, 0, Qt::AlignLeft);
    layoutBoutonSwitch->addStretch(1);

    ///////////////////////////// Aide /////////////////////////////////////
    QLabel *labelAide = new QLabel ("<b>Aide</b><br />Espace : Lancer le chrono/Donner le chrono<br />Entrée : Stopper les deux chronos");
    labelOptions = new QLabel;
    refreshLabel();
	
    QHBoxLayout *layoutAide = new QHBoxLayout;
    layoutAide->addWidget(labelAide);
    layoutAide->addWidget(labelOptions);

	/////////////////////////////Mise en page finale ///////////////////////////
	layoutComplet = new QVBoxLayout;
	layoutComplet->addLayout (layoutChrono);
	layoutComplet->addLayout (layoutBoutonSwitch);
    layoutComplet->addLayout (layoutAide);
    centralWidget->setLayout (layoutComplet);
    setCentralWidget(centralWidget);
	setWindowTitle ("ChronoCheckMate");

    ///////////////////////////// Initialisation du jeu /////////////////
    //lancerDialogueOptions();
    init();

	///////////////////////////// connections ///////////////////////////
    QObject::connect (boutonDemarrer , &QPushButton::clicked , this , &MaFenetre::demarrer);
    QObject::connect (boutonInverser , &QPushButton::clicked , this , &MaFenetre::inverser);
    QObject::connect (boutonChangerCouleur, &QPushButton::clicked , this, &MaFenetre::changerCouleur);
    QObject::connect (boutonArreter , &QPushButton::clicked , this, &MaFenetre::arreter);
    QObject::connect (actionNouveau, &QAction::triggered , this , &MaFenetre::init);
    QObject::connect (actionOptions , &QAction::triggered , this, &MaFenetre::lancerDialogueOptions);
    QObject::connect (actionQuitter , &QAction::triggered , qApp , &QApplication::quit);
    QObject::connect (chrono1, &Chronometre::fin, chrono2, &Chronometre::declareGagnant);
    QObject::connect (chrono2, &Chronometre::fin, chrono1, &Chronometre::declareGagnant);
    QObject::connect (chrono1, &Chronometre::won, this, &MaFenetre::afficheGagnant);
    QObject::connect (chrono2, &Chronometre::won, this, &MaFenetre::afficheGagnant);
    QObject::connect (chrono1, &Chronometre::is_paused, this, &MaFenetre::ajouterTempsJoueur);
    QObject::connect (chrono2, &Chronometre::is_paused, this, &MaFenetre::ajouterTempsJoueur);

}

///////////////// Méthodes ////////////////////
void MaFenetre::etatBoutons (bool etatBoutonStart, bool etatBoutonStop, bool etatBoutonInverser) {
    boutonDemarrer->setEnabled (etatBoutonStart);
    boutonArreter->setEnabled (etatBoutonStop);
    boutonInverser->setEnabled(etatBoutonInverser);
    boutonChangerCouleur->setEnabled(etatBoutonInverser);
}

void MaFenetre::refreshLabel()
{
    QString secondes {};
    labelOptions->setText("<b>Options</b><br />Temps : "
                            + option->tempsDepart().toString("HH:mm:ss")
                            + (option->isTempsSupplementaire() ? " + " + secondes.setNum(option->tempsSupplementaire()) + " secondes" : ""));
}

////////////////// EVENT ////////////////////////
void MaFenetre::closeEvent(QCloseEvent *event)
{
    option->setValue("MAIN/geometry", saveGeometry());
    QMainWindow::closeEvent(event);
}

////////////////// SLOTS ////////////////////////
void MaFenetre::inverser() {
    init();
    QString temp;
    temp = option->nomJoueur1();
    option->setNomJoueur1(option->nomJoueur2());
    option->setNomJoueur2(temp);
    chrono1->modifierNomJoueur(option->nomJoueur1());
    chrono2->modifierNomJoueur(option->nomJoueur2());
}

void MaFenetre::changerCouleur() {
    init();
    chrono1->echangerCouleur(*chrono2);
}

void MaFenetre::demarrer () {
	if (marche == false ) {
        chrono1->reinit ();
        chrono2->reinit ();
        if (chrono1->getCouleur() == Blanc) {
            chrono1->start();
        } else if (chrono2->getCouleur() == Blanc) {
            chrono2->start();
        }
		marche = true;
        etatBoutons (false , true, false);
	}
}

void MaFenetre::switcher () {
	if (marche == true) {
        chrono1->basculer (*chrono2);
	}
}

void MaFenetre::arreter () {
    chrono1->stop();
    chrono2->stop();
	marche = false;
    etatBoutons (false , false, true);
}

void MaFenetre::afficheGagnant(Chronometre *chrono) {
    arreter();
    if (option->son() == true)
        QSound::play(":/ressources/sonnette.wav");
    QMessageBox message;
    QString texte;
    texte = chrono->nomJoueur() + " a gagné la partie au temps.";
    message.setText(texte);
    message.setStandardButtons(QMessageBox::Ok);
    message.exec();
}

void MaFenetre::init () {
    chrono1->reinit();
    chrono2->reinit();
    marche = false;
    etatBoutons (true , false, true);
}

void MaFenetre::lancerDialogueOptions() {
    Dialog dialog (*option);
    if (dialog.exec() == Dialog::Accepted) {
        option->setNomJoueur1(dialog.champs_joueur1());
        option->setNomJoueur2(dialog.champs_joueur2());
        option->setTempsDepart(dialog.champs_temps());
        option->setSon(dialog.champs_son());
        option->setTempsSupplementaire(dialog.champs_temps_Supplementaire());
        option->setTempsSupplementaire(dialog.champs_is_temps_supplementaire());
        option->sauverConf();
        refreshLabel();

        chrono1->modifierNomJoueur(option->nomJoueur1());
        chrono1->definirTemps(option->tempsDepart());

        chrono2->modifierNomJoueur(option->nomJoueur2());
        chrono2->definirTemps(option->tempsDepart());

        if (marche==false) {
            init();
        }
    }
}

void MaFenetre::keyReleaseEvent (QKeyEvent * evenement) {
	if (evenement->key() == Qt::Key_Space) {
        if (marche) {
            switcher();
        }
        if ( !marche && boutonDemarrer->isEnabled()) {
            demarrer();
        } else if ( !marche && !boutonDemarrer->isEnabled()) {
            init();
        }
	}
    if (evenement->key() == Qt::Key_Return) {
        arreter();
    }
}

void MaFenetre::ajouterTempsJoueur(Chronometre * chrono)
{
    if (option->isTempsSupplementaire())
    {
        chrono->addSecs(option->tempsSupplementaire());
        chrono->affichage();
    }
}