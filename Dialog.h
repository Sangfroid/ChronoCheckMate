#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
class QLineEdit;
class QTimeEdit;
class QString;
class Options;
class QCheckBox;
class QTime;
class QSpinBox;

class Dialog : public QDialog
{
    Q_OBJECT
public:
    explicit Dialog(Options const& option, QWidget *parent = nullptr);
    ~Dialog();
    QString champs_joueur1 () const;
    QString champs_joueur2 () const;
    bool    champs_son () const;
    QTime   champs_temps () const;
    unsigned int   champs_temps_Supplementaire() const;
    bool    champs_is_temps_supplementaire() const;

signals:

public slots:
 

private:
    QLineEdit* joueur1LineEdit;
    QLineEdit* joueur2LineEdit;
    QTimeEdit* spinTempsDepart;
    QSpinBox*  spinTempsSupplementaire;
    QCheckBox* checkBoxTempsSupp;
    QCheckBox* checkBoxSon;

};

#endif // DIALOG_H
