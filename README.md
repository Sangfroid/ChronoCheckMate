Chronomètre pour jeu d'échecs
===

Rien de compliqué, il s'agit de deux chronomètre activables en alternance pour chronométrer une partie d'échecs.

- Les horloges sont réglées sur 5 minutes par défaut, le temps est réglable par le menu Options, (5 secondes à 3h)
- On peut choisir le nom des joueurs par le menu Options
- On peut désactiver le son par le menu Options
- Une option pour ajouter quelques secondes après chaque coup est disponible (1 à 59 secondes)
- Un bouton d'inversion de côté permet de placer les joueurs d'un côté ou de l'autre de l'échiquier
- Un bouton permet de changer de camp (blancs ou noirs)

