/*
 * MaFenetre.h
 * This file is part of ChronoCheckMate
 *
 * Copyright (C) 2010 - François Drouhard
 *
 * ChronoCheckMate is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * ChronoCheckMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ChronoCheckMate; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef DEF_MAFENETRE
#define DEF_MAFENETRE
 

#include <QMainWindow>
class Chronometre;
class QVBoxLayout;
class QHBoxLayout;
class QPushButton;
class QLineEdit;
class Options;
class QLabel;

class MaFenetre : public QMainWindow { // On hérite de QMainWindow (IMPORTANT)
	Q_OBJECT

	public:
    MaFenetre();
    void etatBoutons (bool etatBoutonStart , bool etatBoutonStop, bool etatBoutonInverser);

	private slots:
	void demarrer ();
	void switcher ();
	void init ();
	void arreter ();
    void lancerDialogueOptions();
    void inverser ();
    void changerCouleur ();
    void afficheGagnant(Chronometre * chrono);
    void ajouterTempsJoueur(Chronometre *chrono);
    void closeEvent(QCloseEvent *event);

	private:
	void keyReleaseEvent (QKeyEvent * evenement);
    void refreshLabel();

	bool marche;
    Options     *option;
    Chronometre *chrono1;
    Chronometre *chrono2;
    QLabel      *labelOptions;
    QHBoxLayout *layoutChrono;
	QHBoxLayout *layoutBoutons;
    QVBoxLayout *layoutComplet;
	QPushButton *boutonDemarrer;
    QPushButton *boutonArreter;
    QPushButton *boutonInverser;
    QPushButton *boutonChangerCouleur;
    QHBoxLayout *layoutBoutonSwitch;
};

#endif

