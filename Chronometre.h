/*
 * Chronometre.h
 * This file is part of ChronoCheckMate
 *
 * Copyright (C) 2010 - François Drouhard
 *
 * ChronoCheckMate is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * ChronoCheckMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ChronoCheckMate; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */
 
#ifndef DEF_CHRONOMETRE
#define DEF_CHRONOMETRE
 

#include <QWidget>
#include <QTime>
class QGroupBox;
class QLabel;
class QHBoxLayout;
class QVBoxLayout;
class QTimer;
class QString;
class QPixmap;

#define INTERVAL 1 // vitesse de défilement (1000ème de secondes)

enum Couleur {
    Blanc,
    Noir,
};

enum Placement { 
    Gauche,
    Droite
};

class Chronometre : public QWidget {
	
	Q_OBJECT
	
	public:
    Chronometre (const QString & nom , Couleur unecouleur, QTime const& tempsDepart, Placement placement = Placement::Gauche, QWidget * parent = nullptr);
	void start ();
	void stop ();
	void affichage ();
	void reinit ();
    void basculer ();
    void basculer (Chronometre & Chrono);
    void echangerCouleur (Chronometre & chrono);
    void setCouleur (Couleur const& color);
    Couleur getCouleur () const;
    void modifierNomJoueur (QString const& joueur);
    void definirTemps (QTime const& tempsDepart);
    void addSecs (unsigned int secondes);
    QString nomJoueur () const;
    uint nombre_coups() const;

    signals:
    void fin(Chronometre *chrono);
    void won(Chronometre * chrono);
    void is_paused(Chronometre *chrono);

    public slots:
    void declareGagnant();

    protected slots:
    void refresh ();
	
	private:
    QLabel *labelPion;
    Couleur couleur;
    QGroupBox *groupBox;
    QPixmap *pion;
    QTime temps;
    QTime msTempsDepart;
	QTimer *timer;
    QVBoxLayout *ecranLayout;
    QLabel *ecranTemps;
    QLabel *labelCoups;
    uint nb_coups;
};

#endif
