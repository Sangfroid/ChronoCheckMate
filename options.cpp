#include "options.h"
#include <QApplication>
#include <QTime>
#include <QString>
#include <QObject>

Options::Options(QObject *parent) :
    QSettings(QSettings::IniFormat, QSettings::UserScope, "FdLibre", "ChronoCheckmate", parent)
{
    
    chargerConf();
}

Options::~Options ()
{

}

void Options::chargerConf()
{
    m_tempsDepart = value("CONFIG/TEMPS_DEPART", QTime(0, 5, 0, 0)).toTime();
    m_nomJoueur1  = value("JOUEURS/NOM_JOUEUR_1", "Joueur 1").toString();
    m_nomJoueur2  = value("JOUEURS/NOM_JOUEUR_2", "Joueur 2").toString();
    m_son         = value("CONFIG/SON", true).toBool();
    m_isTempsSupplementaire = value("CONFIG/IS_TEMPS_SUPP", false).toBool();
    m_tempsSupplementaire   = value("CONFIG/TEMPS_SUPP", 1).toInt();
}

void Options::sauverConf()
{
    setValue("CONFIG/TEMPS_DEPART", tempsDepart());
    setValue("CONFIG/SON", son());
    setValue("JOUEURS/NOM_JOUEUR_1", nomJoueur1());
    setValue("JOUEURS/NOM_JOUEUR_2", nomJoueur2());
    setValue("CONFIG/IS_TEMPS_SUPP", isTempsSupplementaire());
    setValue("CONFIG/TEMPS_SUPP", tempsSupplementaire());
    sync();
}


void Options::setNomJoueur1(QString const& joueur) {
    m_nomJoueur1 = joueur;
}

void Options::setNomJoueur2(QString const& joueur) {
    m_nomJoueur2 = joueur;
}

void Options::setTempsDepart(QTime const& temps) {
    m_tempsDepart = temps;
}

void Options::setTempsSupplementaire(unsigned int secondes) {
    m_tempsSupplementaire = secondes;
}

void Options::setTempsSupplementaire(bool isTempsSupp) {
    m_isTempsSupplementaire = isTempsSupp;
}

void Options::setSon (bool son) {
    m_son = son;
}

QString Options::nomJoueur1() const {
    return m_nomJoueur1;
}

QString Options::nomJoueur2() const {
    return m_nomJoueur2;
}

QTime Options::tempsDepart() const {
    return m_tempsDepart;
}

unsigned int Options::tempsSupplementaire() const {
    return m_tempsSupplementaire;
}

bool Options::isTempsSupplementaire() const {
    return m_isTempsSupplementaire;
}

bool Options::son() const {
    return m_son;
}
