#include "Dialog.h"
#include "options.h"
#include <QBoxLayout>
#include <QLineEdit>
#include <QTimeEdit>
#include <QTime>
#include <QDialogButtonBox>
#include <QLabel>
#include <QFormLayout>
#include <QCheckBox>
#include <QSpinBox>

Dialog::Dialog(Options const& option, QWidget *parent) :
    QDialog(parent)
{
    QHBoxLayout *layoutLineEdit   = new QHBoxLayout;
    QVBoxLayout *layoutDialog     = new QVBoxLayout;
    QFormLayout *layoutForm       = new QFormLayout;
    QDialogButtonBox *boutonJouer = new QDialogButtonBox (QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    checkBoxSon                   = new QCheckBox;
    checkBoxSon->setChecked(option.son());
    spinTempsDepart               = new QTimeEdit;
    spinTempsDepart->setTimeRange(QTime(0,0,5,0), QTime(3,0,0,0));
    spinTempsDepart->setTime(option.tempsDepart());
    spinTempsDepart->setDisplayFormat("HH:mm:ss");

    spinTempsSupplementaire        = new QSpinBox;
    spinTempsSupplementaire->setRange(1, 180);
    spinTempsSupplementaire->setValue(option.tempsSupplementaire());
    spinTempsSupplementaire->setEnabled(option.isTempsSupplementaire());

    checkBoxTempsSupp             = new QCheckBox; 
    checkBoxTempsSupp->setChecked(option.isTempsSupplementaire());

    joueur1LineEdit = new QLineEdit(option.nomJoueur1() , this);
    joueur2LineEdit = new QLineEdit(option.nomJoueur2() , this);
    joueur1LineEdit->selectAll();

    layoutLineEdit->addWidget(joueur1LineEdit);
    layoutLineEdit->addWidget(joueur2LineEdit);

    layoutForm->addRow("Son", checkBoxSon);
    layoutForm->addRow("Temps de départ", spinTempsDepart);
    layoutForm->addRow("Temps supplémentaire", checkBoxTempsSupp);
    layoutForm->addRow("Secondes supplémentaires (1 - 180)", spinTempsSupplementaire);

    layoutDialog->addLayout(layoutLineEdit);
    layoutDialog->addLayout(layoutForm);
    layoutDialog->addWidget(boutonJouer);
    setLayout(layoutDialog);

    QObject::connect(boutonJouer , &QDialogButtonBox::accepted , this, &Dialog::accept);
    QObject::connect(boutonJouer , &QDialogButtonBox::rejected , this, &Dialog::close);
    QObject::connect(checkBoxTempsSupp, &QCheckBox::stateChanged, spinTempsSupplementaire, &QTimeEdit::setEnabled);
}

Dialog::~Dialog() {

}

QString Dialog::champs_joueur1() const
{
    return joueur1LineEdit->text();
}

QString Dialog::champs_joueur2() const
{
    return joueur2LineEdit->text();
}

bool Dialog::champs_son() const
{
    return checkBoxSon->isChecked();
}

QTime Dialog::champs_temps() const
{
    return spinTempsDepart->time();
}

unsigned int Dialog::champs_temps_Supplementaire() const
{
    return spinTempsSupplementaire->value();
}

bool Dialog::champs_is_temps_supplementaire() const
{
    return checkBoxTempsSupp->isChecked();
}